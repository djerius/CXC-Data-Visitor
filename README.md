# NAME

CXC::Data::Visitor - Invoke a callback on every element at every level of a data structure.

# VERSION

version 0.07

# SYNOPSIS

    use CXC::Data::Visitor 'visit', 'RESULT_CONTINUE';

    my $hoh = { fruit => { berry => 'purple' }, };

    visit(
        $hoh,
        sub {
            my ( $key, $vref ) = @_;
            $$vref = 'blue' if $key eq 'berry';
            return RESULT_CONTINUE;
        } );

    say $hoh->{fruit}{berry}    # 'blue'

# DESCRIPTION

**CXC::Data::Visitor** provides a means of performing a depth first
traversal of a data structure.  There are similar modules on CPAN
(["SEE ALSO"](#see-also)); this module provides a few extras:

- The traversal may be aborted.
- A container (hash or array) may be immediately revisited if the
callback requests it.
- An element whose value is a container may be revisited after the container is
visited if the callback requests it.
- User selectable behavior upon detection of a traversal cycle.
- The complete path from the structure route to an element (both the
ancestor containers and the keys and indexes required to traverse the
path) is available.

# SUBROUTINES

## visit

    ( $completed, $context, $metadata ) = visit( $struct, $callback, %opts );

Perform a depth-first traversal of **$struct**, invoking **$callback**
on containers (hashes and arrays) and terminal elements in **$struct**.

- Blessed hashes or arrays are treated as terminal elements and are not
further traversed.
- Hashes are traversed in sorted key order.
- Cycles are detected upon traversing a container a second time in a
depth first search.
- Containers that can be reached multiple times without cycling, e.g.

        %hash = ( a => { b => 1 }, );
        $hash{c} = $hash{a};

    are visited once per parent, e.g.

        {a}, {a}{b}, {a}{b}[0]
        {c}, {c}{b}, {c}{b}[0]

- **$callback** is first invoked on a container and then its
elements.  Given

        { a => { b => [ 0 ], c => 2 } }

    the callback order is

        {a}, {a}{b}, {a}{b}[0], {a}{c}

["visit"](#visit) returns the following:

- **$completed**  => _Boolean_

    _true_ if all elements were visited, _false_ if
    **$callback** requested a premature return.

- **$context**

    The variable of the same name passed to **$callback**; see the ["context"](#context) option to ["visit"](#visit).

- **$metadata** => _hash_

    collected metadata. See ["$metadata"](#metadata) below.

**$callback** will be called as:

    $handle_return = $callback->( $kydx, $vref, $context, \%metadata );

and should return one of the following (see ["EXPORTS"](#exports) to import the constants):

- RESULT\_CONTINUE

    The process of visiting elements should continue.

- RESULT\_RETURN

    ["visit"](#visit) should return immediately to the caller.

- RESULT\_STOP\_DESCENT

    If the current element is a container, do not visit the container's contents
    (containers are visited before their contents).

    For leaf elements, this is equivalent to ["RESULT\_CONTINUE"](#result_continue).

- RESULT\_REVISIT\_CONTAINER

    Further processing of the elements in the current container should stop
    and the container should be revisited.  This allows ["$callback"](#callback) to
    modify the container and have it reprocessed.

    To avoid inadvertent infinite loops, a finite number of revisits
    is allowed during a traversal of a container (see ["revisit\_limit"](#revisit_limit)).
    Containers with multiple parents are traversed once per parent;
    The limit is reset for each traversal.

- RESULT\_REVISIT\_ELEMENT

    If the value of this element is a container, it should be revisited
    (by calling ["$callback"](#callback)) after its value is visited.  This
    allows post-processing results when travelling back up the structure.

    During the initial visit

        $metadataa->{pass} & PASS_VISIT_ELEMENT

    will be true.  During the followup visit

        $metadata->{pass} & PASS_REVISIT_ELEMENT

    will be true and ["$callback"](#callback) may only return values of
    **RESULT\_RETURN** and **RESULT\_CONTINUE**; any other values will cause
    an exception.

    The ordered contents of the [$metadata{path}](https://metacpan.org/pod/%24metadata%7Bpath%7D) array uniquely
    identify an element, so may be used to track elements using external data
    structures.  Do not depend upon reference addresses remaining constant.

["$callback"](#callback) is passed

- **$kydx**

    the key or index into **$container** of the element being visited.

- **$vref**

    a reference to the value of the element being visited.  Use **$$vref**
    to get the actual value.

- **$context**

    See the ["context"](#context) option to ["visit"](#visit).

- **$metadata**

    A hash of state information kept by **CXC::Data::Visitor**, but which
    may be of interest to the callback:

    - **container**

        a reference to the hash or array which contains the element being visited.

    - **path**

        An array which contains the path (keys and indices) used to arrive
        at the current element from **$struct**.

    - **ancestors**

        An array contains the ancestor containers of the current element.

- **pass**

    A constant indicating the current visit pass through an element.

**%opts** may contain the following entries:

- **context**

    Arbitrary data to be passed to ["$callback"](#callback) via the `$context` argument. Use it
    for whatever you'd like.  If not specified, it defaults to a freshly created hash.

- **cycle** => CYCLE\_TRUNCATE | CYCLE\_DIE | CYCLE\_CONTINUE | &lt;$coderef>

    How to handle cycles in the data structure.

    See ["EXPORTS"](#exports) to import the constant values.

    - CYCLE\_DIE

        Throw an exception (the default).

    - CYCLE\_CONTINUE

        Pretend we haven't seen it before. Will cause stack exhaustion if
        **$callback** does handle this.

    - CYCLE\_TRUNCATE

        Truncate before entering the cycle a second time.

    - _$coderef_

        Examine the situation and request a particular resolution.
        **$coderef** is called as

            $coderef->( $container, $context, $metadata );

        where **$container** is the hash or array which has already been
        traversed. See below for ["$context"](#context) and ["$metadata"](#metadata).

        **$coderef** should return one of **CYCLE\_DIE**, **CYCLE\_CONTINUE**, or **CYCLE\_TRUNCATE**,
        indicating what should be done.

- _visit_ => VISIT\_HASH | VISIT\_ARRAY | VISIT\_CONTAINER | VISIT\_LEAF | VISIT\_ALL

    The parts of the structure that will trigger a callback.
    See ["EXPORTS"](#exports) to import the constants.

    - VISIT\_CONTAINER

        Invoke ["$callback"](#callback) on containers (either hashes or arrays).  For
        example, the elements in the following structure

            $struct = { a => { b => 1, c => [ 2, 3 ] } }

        passed to ["$callback"](#callback) are:

            a => {...}  # $struct->{a}
            c => [...]  # $struct->{c}

    - VISIT\_ARRAY
    - VISIT\_HASH

        Only visit containers of the given type.

    - VISIT\_LEAF

        Invoke ["$callback"](#callback) on terminal (leaf) elements.  For example, the
        elements in the following structure

            $struct = { a => { b => 1, c => [ 2, 3 ] } }

        passed to ["$callback"](#callback) are:

            b => 1  # $struct->{a}{b}
            0 => 2  # $struct->{a}{c}[0]
            1 => 3  # $struct->{a}{c}[1]

    - VISIT\_ALL

        Invoke ["$callback"](#callback) on all elements.  This is the default.

- revisit\_limit

    If ["$callback"](#callback) returns **RESULT\_REVISIT\_CONTAINER**, then the parent container
    of the element is re-scanned for its elements and revisited.  To avoid an inadvertent
    infinite loop, an exception is thrown if the parent container is revisited more
    than this number of times.  It defaults to 10;

# EXPORTS

This module uses [Exporter::Tiny](https://metacpan.org/pod/Exporter%3A%3ATiny), which provides enhanced import utilities.

The following symbols may be exported:

    visit

    VISIT_CONTAINER VISIT_LEAF VISIT_ALL

    CYCLE_DIE CYCLE_CONTINUE CYCLE_TRUNCATE

    RESULT_RETURN RESULT_CONTINUE
    RESULT_REVISIT_CONTAINER RESULT_REVISIT_ELEMENT
    RESULT_STOP_DESCENT

    PASS_VISIT_ELEMENT PASS_REVISIT_ELEMENT

The available tags and their respective imported symbols are:

- **all**

    Import all symbols.

- **results**

        RESULT_RETURN RESULT_CONTINUE
        RESULT_REVISIT_CONTAINER RESULT_REVISIT_ELEMENT
        RESULT_STOP_DESCENT

- **cycles**

        CYCLE_DIE CYCLE_CONTINUE CYCLE_TRUNCATE

- **visits**

        VISIT_CONTAINER VISIT_LEAF VISIT_ALL

- **passes**

        PASS_VISIT_ELEMENT PASS_REVISIT_ELEMENT

- **constants**

    Import tags `results`, `cycles`, `visits`.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-data-visitor@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Data-Visitor](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Data-Visitor)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-data-visitor

and may be cloned from

    https://gitlab.com/djerius/cxc-data-visitor.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [Data::Rmap](https://metacpan.org/pod/Data%3A%3ARmap)
- [Data::Traverse](https://metacpan.org/pod/Data%3A%3ATraverse)
- [Data::Visitor::Lite](https://metacpan.org/pod/Data%3A%3AVisitor%3A%3ALite)
- [Data::Visitor::Tiny](https://metacpan.org/pod/Data%3A%3AVisitor%3A%3ATiny)
- [Data::Walk](https://metacpan.org/pod/Data%3A%3AWalk)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2024 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
